const axios = require('axios')

class Safa {
    constructor (api_key, units) {
        this.api_key = api_key
        this.units = units
    }

    calculateDistance = async (pickup, destinations) => {
        let url = 'https://maps.googleapis.com/maps/api/distancematrix/json'
        let response = await axios.get(url, {
            params: {
                destinations: destinations.map(c => `${c.latitude},${c.longitude}`).join('|'),
                origins: `${pickup.latitude},${pickup.longitude}`,
                units: this.units,
                key: this.api_key
            }
        })
        .catch(err => console.error(err))

        if (response.data.status === 'OK') {
            let { rows } = response.data
            
            return rows[0].elements
        } else {
            throw new Error('Problem was encountered')
        }
    }

    sort = async (pickup, destinations) => {
        let distances = await this.calculateDistance(pickup, destinations)

        let dests = destinations.map((dest, index) => ({
            ...dest,
            distance: distances[index].distance.value
        }))

        dests.sort((a, b) => (a.distance > b.distance) ? 1 : -1)

        return dests
    }

    totalDistance = async (pickup, destinations) => {
        let values = []
        let prev = pickup

        await asyncForEach(destinations, async dest => {
            let distances = await this.calculateDistance(prev, [{ latitude: dest.latitude, longitude: dest.longitude}])
            values.push(distances[0].distance.value)
            prev = dest
        }) 

        return values.reduce((sum, x) => sum + x)
    }
}

module.exports = Safa